<?php
/**
 * Author:          Andrei Baicus <andrei@themeisle.com>
 * Created on:      28/08/2018
 *
 * @package Neve
 */
//do_action( 'neve_do_single_post', 'single-post' );
?>

<header class="entry-header">
    <?php the_title('<h2 class="entry-title text-center">', '</h2>'); ?>
</header><!-- .entry-header -->

<div class="single-post-thumbnail"><?php echo get_the_post_thumbnail($post->ID, 'large'); ?></div>

<div class="entry-content text-center">
    <?php the_content(); ?>
    <?php
    wp_link_pages(array(
        'before' => '<div class="page-links">' . __('Pages:', 'neve'),
        'after' => '</div>',
    ));
    ?>
</div><!-- .entry-content -->
