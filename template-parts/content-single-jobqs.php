<?php
/**
 * Author:          Andrei Baicus <andrei@themeisle.com>
 * Created on:      28/08/2018
 *
 * @package Neve
 */
//do_action( 'neve_do_single_post', 'single-post' );
?>

   <header class="entry-header">
        <?php the_title('<h2 class="entry-title text-center">', '</h2>'); ?>
    </header><!-- .entry-header -->
    <?php echo get_the_post_thumbnail($post->ID, 'large'); ?>

    <div class="entry-content text-center">
        <?php the_content(); ?>
        <?php
        wp_link_pages(array(
            'before' => '<div class="page-links">' . __('Pages:', 'neve'),
            'after' => '</div>',
        ));
        ?>
    </div><!-- .entry-content -->


<div class="mcq-wrapper">

    <div class="mcq-header-btn-area">
        <div class="left-back-btn-area">
            <a href="#"><button type="button" class="btn btn-secondary m-btn page-item"><i class="fa fa-arrow-circle-left"></i> Back to Question Type</button></a>
        </div>
        <div class="right-read-btn-area">
            <button type="button" id="checkallans" class="btn btn-success m-btn page-item btn-sticky">Reading Mode</button>
        </div>
    </div>

    <ul class="mcq-tabs-nav">
        <li><a href="#bangla-mcq">Bangla</a></li>
        <li><a href="#english-mcq">English</a></li>
        <li><a href="#math-mcq">Math</a></li>
        <li><a href="#gk-mcq">General Knowledge</a></li>
    </ul>

    <div class="mcq-tabs-stage">
        <div class="tabs-body" id="bangla-mcq">
            <!-- question section start -->
            <div class="row onclickshow">
                <?php
                $b_mcqs = get_field('b_mcq');
                if ($b_mcqs) {
                    $bangla_no = 0;
                    foreach ($b_mcqs as $b_mcq) {
                        $bangla_no++ ?>

                        <div class="col-sm-12">
                            <div class="card card-custom custom-color">

                                <div class="card-header card-custom-header">
                                    <h5 class="card-title">
                                        <a href="#"><?php echo $bangla_no . ". ";
                                            echo $b_mcq['qn']; ?></a>
                                    </h5>
                                </div>

                                <div class="card-body">
                                    <div class="card-text m-card-text">
                                        <div class="row ansonclick" style="cursor: pointer">


                                            <?php
                                            $b_mcq_ans1 = $b_mcq['a1'] === $b_mcq['ra'] ? 'rightAns' : 'wrongAns';
                                            echo '<div class="col-sm-6">
                                                <p class="answer  ' . $b_mcq_ans1 . '">
                                                    <i class="' . $b_mcq_ans1 . 'Opt fa fa-eye"
                                                       aria-hidden="true"></i>
                                                    ' . $b_mcq['a1'] . '
                                                </p>
                                            </div>';

                                            $b_mcq_ans2 = $b_mcq['a2'] === $b_mcq['ra'] ? 'rightAns' : 'wrongAns';
                                            echo '<div class="col-sm-6">
                                                <p class="answer  ' . $b_mcq_ans2 . '">
                                                    <i class="' . $b_mcq_ans2 . 'Opt fa fa-eye"
                                                       aria-hidden="true"></i>
                                                    ' . $b_mcq['a2'] . '
                                                </p>
                                            </div>';

                                            $b_mcq_ans3 = $b_mcq['a3'] === $b_mcq['ra'] ? 'rightAns' : 'wrongAns';
                                            echo '<div class="col-sm-6">
                                                <p class="answer  ' . $b_mcq_ans3 . '">
                                                    <i class="' . $b_mcq_ans3 . 'Opt fa fa-eye"
                                                       aria-hidden="true"></i>
                                                    ' . $b_mcq['a3'] . '
                                                </p>
                                            </div>';

                                            $b_mcq_ans4 = $b_mcq['a4'] === $b_mcq['ra'] ? 'rightAns' : 'wrongAns';
                                            echo '<div class="col-sm-6">
                                                <p class="answer  ' . $b_mcq_ans4 . '">
                                                    <i class="' . $b_mcq_ans4 . 'Opt fa fa-eye"
                                                       aria-hidden="true"></i>
                                                    ' . $b_mcq['a4'] . '
                                                </p>
                                            </div>';

                                            ?>

                                        </div>
                                    </div>
                                    <span class="badge badge-success check_ans check_ans_opt" style="display: none"><i class="fa fa-eye" aria-hidden="true"></i>ShowAns</span>
<!--                                    <span class="badge badge-success check_ans check_ans_opt" style="display: none"><i class="fa fa-eye" aria-hidden="true"></i>ShowAns</span>-->
                                </div>

                            </div>
                        </div>

                    <?php }

                } ?>





            </div>
            <!-- question section end -->
        </div>
        <div id="english-mcq" class="tabs-body" >
            <!-- question section start -->
            <div class="row onclickshow">
                <?php
                $e_mcqs = get_field('e_mcq');
                if ($e_mcqs) {
                    $english_no = 0;
                    foreach ($e_mcqs as $e_mcq) {
                        $english_no++ ?>

                        <div class="col-sm-12">
                            <div class="card card-custom custom-color">

                                <div class="card-header card-custom-header">
                                    <h5 class="card-title">
                                        <a href="#"><?php echo $english_no . ". ";
                                            echo $e_mcq['qn']; ?></a>
                                    </h5>
                                </div>

                                <div class="card-body">
                                    <div class="card-text m-card-text">
                                        <div class="row ansonclick" style="cursor: pointer">


                                            <?php
                                            $e_mcq_ans1 = $e_mcq['a1'] === $e_mcq['ra'] ? 'rightAns' : 'wrongAns';
                                            echo '<div class="col-sm-6">
                                                <p class="answer  ' . $e_mcq_ans1 . '">
                                                    <i class="' . $e_mcq_ans1 . 'Opt fa fa-eye"
                                                       aria-hidden="true"></i>
                                                    ' . $e_mcq['a1'] . '
                                                </p>
                                            </div>';

                                            $e_mcq_ans2 = $e_mcq['a2'] === $e_mcq['ra'] ? 'rightAns' : 'wrongAns';
                                            echo '<div class="col-sm-6">
                                                <p class="answer  ' . $e_mcq_ans2 . '">
                                                    <i class="' . $e_mcq_ans2 . 'Opt fa fa-eye"
                                                       aria-hidden="true"></i>
                                                    ' . $e_mcq['a2'] . '
                                                </p>
                                            </div>';

                                            $e_mcq_ans3 = $e_mcq['a3'] === $e_mcq['ra'] ? 'rightAns' : 'wrongAns';
                                            echo '<div class="col-sm-6">
                                                <p class="answer  ' . $e_mcq_ans3 . '">
                                                    <i class="' . $e_mcq_ans3 . 'Opt fa fa-eye"
                                                       aria-hidden="true"></i>
                                                    ' . $e_mcq['a3'] . '
                                                </p>
                                            </div>';

                                            $e_mcq_ans4 = $e_mcq['a4'] === $e_mcq['ra'] ? 'rightAns' : 'wrongAns';
                                            echo '<div class="col-sm-6">
                                                <p class="answer  ' . $e_mcq_ans4 . '">
                                                    <i class="' . $e_mcq_ans4 . 'Opt fa fa-eye"
                                                       aria-hidden="true"></i>
                                                    ' . $e_mcq['a4'] . '
                                                </p>
                                            </div>';

                                            ?>

                                        </div>
                                    </div>
                                    <span class="badge badge-success check_ans check_ans_opt" style="display: none"><i class="fa fa-eye" aria-hidden="true"></i>ShowAns</span>
                                </div>

                            </div>
                        </div>

                    <?php }

                } ?>





            </div>
            <!-- question section end -->
        </div>
        <div id="math-mcq" class="tabs-body" >
            <!-- question section start -->
            <div class="row onclickshow">
                <?php
                $m_mcqs = get_field('m_mcq');
                if ($m_mcqs) {
                    $math_no = 0;
                    foreach ($m_mcqs as $m_mcq) {
                        $math_no++ ?>

                        <div class="col-sm-12">
                            <div class="card card-custom custom-color">

                                <div class="card-header card-custom-header">
                                    <h5 class="card-title">
                                        <a href="#"><?php echo $math_no . ". ";
                                            echo $m_mcq['qn']; ?></a>
                                    </h5>
                                </div>

                                <div class="card-body">
                                    <div class="card-text m-card-text">
                                        <div class="row ansonclick" style="cursor: pointer">


                                            <?php
                                            $m_mcq_ans1 = $m_mcq['a1'] === $m_mcq['ra'] ? 'rightAns' : 'wrongAns';
                                            echo '<div class="col-sm-6">
                                                <p class="answer  ' . $m_mcq_ans1 . '">
                                                    <i class="' . $m_mcq_ans1 . 'Opt fa fa-eye"
                                                       aria-hidden="true"></i>
                                                    ' . $m_mcq['a1'] . '
                                                </p>
                                            </div>';

                                            $m_mcq_ans2 = $m_mcq['a2'] === $m_mcq['ra'] ? 'rightAns' : 'wrongAns';
                                            echo '<div class="col-sm-6">
                                                <p class="answer  ' . $m_mcq_ans2 . '">
                                                    <i class="' . $m_mcq_ans2 . 'Opt fa fa-eye"
                                                       aria-hidden="true"></i>
                                                    ' . $m_mcq['a2'] . '
                                                </p>
                                            </div>';

                                            $m_mcq_ans3 = $m_mcq['a3'] === $m_mcq['ra'] ? 'rightAns' : 'wrongAns';
                                            echo '<div class="col-sm-6">
                                                <p class="answer  ' . $m_mcq_ans3 . '">
                                                    <i class="' . $m_mcq_ans3 . 'Opt fa fa-eye"
                                                       aria-hidden="true"></i>
                                                    ' . $m_mcq['a3'] . '
                                                </p>
                                            </div>';

                                            $m_mcq_ans4 = $m_mcq['a4'] === $m_mcq['ra'] ? 'rightAns' : 'wrongAns';
                                            echo '<div class="col-sm-6">
                                                <p class="answer  ' . $m_mcq_ans4 . '">
                                                    <i class="' . $m_mcq_ans4 . 'Opt fa fa-eye"
                                                       aria-hidden="true"></i>
                                                    ' . $m_mcq['a4'] . '
                                                </p>
                                            </div>';

                                            ?>

                                        </div>
                                    </div>
                                    <span class="badge badge-success check_ans check_ans_opt" style="display: none"><i class="fa fa-eye" aria-hidden="true"></i>ShowAns</span>
                                </div>

                            </div>
                        </div>

                    <?php }

                } ?>





            </div>
            <!-- question section end -->
        </div>
        <div id="gk-mcq" class="tabs-body" >
            <!-- question section start -->
            <div class="row onclickshow">
                <?php
                $gk_mcqs = get_field('gk_mcq');
                if ($gk_mcqs) {
                    $gk_no = 0;
                    foreach ($gk_mcqs as $gk_mcq) {
                        $gk_no++ ?>

                        <div class="col-sm-12">
                            <div class="card card-custom custom-color">

                                <div class="card-header card-custom-header">
                                    <h5 class="card-title">
                                        <a href="#"><?php echo $gk_no . ". ";
                                            echo $gk_mcq['qn']; ?></a>
                                    </h5>
                                </div>

                                <div class="card-body">
                                    <div class="card-text m-card-text">
                                        <div class="row ansonclick" style="cursor: pointer">


                                            <?php
                                            $gk_mcq_ans1 = $gk_mcq['a1'] === $gk_mcq['ra'] ? 'rightAns' : 'wrongAns';
                                            echo '<div class="col-sm-6">
                                                <p class="answer  ' . $gk_mcq_ans1 . '">
                                                    <i class="' . $gk_mcq_ans1 . 'Opt fa fa-eye"
                                                       aria-hidden="true"></i>
                                                    ' . $gk_mcq['a1'] . '
                                                </p>
                                            </div>';

                                            $gk_mcq_ans2 = $gk_mcq['a2'] === $gk_mcq['ra'] ? 'rightAns' : 'wrongAns';
                                            echo '<div class="col-sm-6">
                                                <p class="answer  ' . $gk_mcq_ans2 . '">
                                                    <i class="' . $gk_mcq_ans2 . 'Opt fa fa-eye"
                                                       aria-hidden="true"></i>
                                                    ' . $gk_mcq['a2'] . '
                                                </p>
                                            </div>';

                                            $gk_mcq_ans3 = $gk_mcq['a3'] === $gk_mcq['ra'] ? 'rightAns' : 'wrongAns';
                                            echo '<div class="col-sm-6">
                                                <p class="answer  ' . $gk_mcq_ans3 . '">
                                                    <i class="' . $gk_mcq_ans3 . 'Opt fa fa-eye"
                                                       aria-hidden="true"></i>
                                                    ' . $gk_mcq['a3'] . '
                                                </p>
                                            </div>';

                                            $gk_mcq_ans4 = $gk_mcq['a4'] === $gk_mcq['ra'] ? 'rightAns' : 'wrongAns';
                                            echo '<div class="col-sm-6">
                                                <p class="answer  ' . $gk_mcq_ans4 . '">
                                                    <i class="' . $gk_mcq_ans4 . 'Opt fa fa-eye"
                                                       aria-hidden="true"></i>
                                                    ' . $gk_mcq['a4'] . '
                                                </p>
                                            </div>';

                                            ?>

                                        </div>
                                    </div>
                                    <span class="badge badge-success check_ans check_ans_opt" style="display: none"><i class="fa fa-eye" aria-hidden="true"></i>ShowAns</span>
                                </div>

                            </div>
                        </div>

                    <?php }

                } ?>





            </div>
            <!-- question section end -->
        </div>

    </div>
</div>

<style>
    .mcq-header-btn-area{
        text-align: center;
        margin-bottom: 30px;
    }
    .left-back-btn-area{
        display: inline-block;
    }
    .right-read-btn-area{
        display: inline-block;
    }
    .mcq-wrapper{
        margin: 40px 0 0;
    }
    .mcq-tabs-nav{
        text-align: center;
        margin: 20px 0;
    }
    .mcq-tabs-nav li {
        display: inline-block;
    }
    .mcq-tabs-nav li:first-child a {

    }
    .mcq-tabs-nav li:last-child a {
    }
    .mcq-tabs-nav a {

        font-size: 14px;
        text-decoration: none;
        transition: background 250ms;
        display: inline-block;
        text-transform: uppercase;
        margin: 0 3px 6px;
        height: 38px;
        min-width: 38px;
        border: 2px solid #38c695;
        line-height: 34px;
        padding: 0 15px;
        color: #000;
        font-weight: 700;
        letter-spacing: .03em;

    }
    .mcq-tabs-nav a:hover {
        color: #ff7b29;
    }
    .tab-active a {
        background: #38c695;
        color: #fff;
        cursor: default;
    }
    .mcq-tabs-stage {
        border-radius: 0 0 6px 6px;
        border-top: 0;
        clear: both;
        padding: 24px 0px;
        position: relative;
        top: -1px;
    }
    /*//style for bottom result*/
      .temp-result.temp-result-bottom {
          background: #292929;
          color: #fff;
          position: fixed;
          bottom: 0px;
          z-index: 999;
          left: 50%;
          margin-left: -160px;
          padding: 5px 20px;
          border-top-left-radius: 4px;
          border-top-right-radius: 4px;
      }

    .card {
        position: relative;
        display: -ms-flexbox;
        display: flex;
        -ms-flex-direction: column;
        flex-direction: column;
        min-width: 0;
        word-wrap: break-word;
        background-color: #fff;
        background-clip: border-box;
        border: 1px solid rgba(0, 0, 0, 0.125);
        border-radius: 0.25rem;
        margin-bottom: 30px;
    }
    .card-header:first-child {
        border-radius: calc(0.25rem - 1px) calc(0.25rem - 1px) 0 0;
    }
    .card-header {
        padding: 0.75rem 1.25rem;
        margin-bottom: 0;
        background-color: rgba(0, 0, 0, 0.03);
        border-bottom: 1px solid rgba(0, 0, 0, 0.125);
    }
    .card-body {
        -ms-flex: 1 1 auto;
        flex: 1 1 auto;
        padding: 1.25rem;
    }

    .btn-success,
    .badge-success{
        background-color: #1e7e34;
    }
    .showAns-custom-color-success,
    .custom-color-success{
        color: #1e7e34;
    }
    .btn-danger,
    .badge-danger{
        background-color: #dc3545;
    }
    .custom-color-danger{
        color: #dc3545;
    }
    .badge {
        border-radius: 4px;
        padding: 0 5px;
        color: #fff;
    }
</style>






<div class="temp-result temp-result-bottom">
    RightAns: <b id="positive">0</b> |
    WrongAns: <b id="nagative">0</b> |
    Result: <b id="total">0</b>/<b id="total_ques">0</b>
</div>


<!-- pagination script -->
<script type="text/javascript">

    jQuery(document).ready(function () {

        //tabs
        // Show the first tab by default
        jQuery('.mcq-tabs-stage .tabs-body').hide();
        jQuery('.mcq-tabs-stage .tabs-body:first').show();
        jQuery('.mcq-tabs-nav li:first').addClass('tab-active');

        // Change tab class and display content
        jQuery('.mcq-tabs-nav a').on('click', function(event){
            event.preventDefault();
            jQuery('.mcq-tabs-nav li').removeClass('tab-active');
            jQuery(this).parent().addClass('tab-active');
            jQuery('.mcq-tabs-stage .tabs-body').hide();
            jQuery(jQuery(this).attr('href')).show();
        });



        jQuery('.pagination li:nth-child(2)').addClass('active disable-link');
        jQuery('#prevHrefTop').attr('href', jQuery('.page-item.active').prev().find('a').attr('href'));
        if (!jQuery('.page-item.active').prev().find('a').attr('href')) {
            jQuery('#prevHrefTop').parent().addClass('disabled');
        }
        jQuery('#nextHrefTop').attr('href', jQuery('.page-item.active').next().find('a').attr('href'));
        if (!jQuery('.page-item.active').next().find('a').attr('href')) {
            jQuery('#nextHrefTop').parent().addClass('disabled');
        }
        jQuery('#prevHrefBottom').attr('href', jQuery('.page-item.active').prev().find('a').attr('href'));
        if (!jQuery('.page-item.active').prev().find('a').attr('href')) {
            jQuery('#prevHrefBottom').parent().addClass('disabled');
        }
        jQuery('#nextHrefBottom').attr('href', jQuery('.page-item.active').next().find('a').attr('href'));
        if (!jQuery('.page-item.active').next().find('a').attr('href')) {
            jQuery('#nextHrefBottom').parent().addClass('disabled');
        }

        var positive = 0;
        var nagative = 0;
        var nagative_count = 0;
        var total = 0;
        var total_ques = jQuery('.ansonclick').length;
        jQuery('#total_ques').html(total_ques);

        jQuery('#checkallans').click(function () {

            positive = 0;
            nagative = 0;
            nagative_count = 0;
            total = 0;
            jQuery('#positive').html(positive);
            jQuery('#nagative').html(nagative);
            jQuery('#total').html(0);
            jQuery('.check_ans').hide(1000);
            var wrongAnsOpt = jQuery(".wrongAnsOpt").attr("class").split(' ');
            var rightAnsOpt = jQuery(".rightAnsOpt").attr("class").split(' ');

            jQuery('#checkallans').toggleClass('btn-success btn-danger');

            if (jQuery("#checkallans").text() == "Reading Mode") {
                jQuery("#checkallans").text("Self Test Mode");
                jQuery('.wrongAnsOpt.fa-eye').toggleClass('fa-eye fa-times');
                jQuery('.rightAnsOpt.fa-eye').toggleClass('fa-eye fa-check');
                jQuery('.rightAnsOpt').parent().addClass('showAns-custom-color-success');
                jQuery('.answer_dash input').hide(1000);
                jQuery('.hide_dash_ans').show(1000);
                jQuery('.temp-result').hide(1000);
            } else {
                jQuery('.temp-result').css('background-color', 'gray');
                jQuery("#checkallans").text("Reading Mode");
                jQuery('.wrongAnsOpt.fa-times').toggleClass('fa-eye fa-times');
                jQuery('.rightAnsOpt.fa-check').toggleClass('fa-eye fa-check');
                jQuery('.rightAnsOpt').parent().removeClass('showAns-custom-color-success');
                jQuery('.hide_dash_ans').hide(1000);
                jQuery('.answer_dash input').show(1000).removeAttr('disabled').css('border-color', '#ced4da').val('');
                jQuery('.temp-result').show(1000);
                if (jQuery('.check_ans').html() == '<i class="fa fa-eye-slash" aria-hidden="true"></i>HideAns') {

                    jQuery('.check_ans').html('<i class="fa fa-eye" aria-hidden="true"></i>ShowAns');
                    jQuery('.check_ans').removeClass('badge-danger');
                    jQuery('.check_ans').addClass('badge-success');
                }
                jQuery('.ansonclick').css('pointer-events', 'all');
            }
        });
        jQuery('.check_ans_opt').click(function () {

            jQuery(this).parents('.card-body').find('.wrongAnsOpt').toggleClass('fa-times fa-eye');
            jQuery(this).parents('.card-body').find('.rightAnsOpt').toggleClass('fa-check fa-eye');
            jQuery(this).toggleClass('badge-success badge-danger');
            if (jQuery(this).html() == '<i class="fa fa-eye" aria-hidden="true"></i>ShowAns') {
                jQuery(this).html('<i class="fa fa-eye-slash" aria-hidden="true"></i>HideAns');
                jQuery(this).parent().find('.rightAnsOpt').parent().addClass('showAns-custom-color-success');
            } else {
                jQuery(this).html('<i class="fa fa-eye" aria-hidden="true"></i>ShowAns');
                jQuery(this).parent().find('.rightAnsOpt').parent().removeClass('showAns-custom-color-success');
            }
        });
        jQuery('.answer').click(function () {
            jQuery(this).find('.wrongAnsOpt').toggleClass('fa-times fa-eye');
            jQuery(this).find('.wrongAnsOpt').parent().addClass('custom-color-danger');
            jQuery(this).find('.rightAnsOpt').toggleClass('fa-check fa-eye');
            jQuery(this).find('.rightAnsOpt').parent().addClass('custom-color-success');
            var that = jQuery(this);
            setTimeout(function () {
                jQuery(that).find('.wrongAnsOpt').toggleClass('fa-eye fa-times');
                jQuery(that).find('.wrongAnsOpt').parent().removeClass('custom-color-danger');
                jQuery(that).find('.rightAnsOpt').toggleClass('fa-eye fa-check');
                jQuery(that).find('.rightAnsOpt').parent().removeClass('custom-color-success');
            }, 1000);
        });
        jQuery('.answer_dash input').on('keyup keypress blur change', function () {
            var data_ans = jQuery(this).parent().attr('ans-data');
            var type_data = jQuery(this).val();

            if (data_ans.length <= type_data.length) {
                jQuery(this).parents('.card-body').find('.dash_ans_opt').show(1000);
                jQuery(this).attr('disabled', 'disabled');


                if (data_ans == type_data) {

                    jQuery(this).css('border-color', 'green');
                    positive = positive + 1;
                    total = positive - nagative;
                    jQuery('#positive').html(positive);
                    jQuery('#total').html(total);
                    if (total < total_ques * .55) {
                        jQuery('.temp-result').css('background-color', 'red');
                    } else {
                        jQuery('.temp-result').css('background-color', 'green');
                    }
                } else {

                    jQuery(this).css('border-color', 'red');
                    nagative = nagative + .25;
                    nagative_count = nagative_count + 1;
                    total = positive - nagative;
                    jQuery('#nagative').html(nagative_count);
                    jQuery('#total').html(total);
                    if (total < total_ques * .55) {
                        jQuery('.temp-result').css('background-color', 'red');
                    } else {
                        jQuery('.temp-result').css('background-color', 'green');
                    }
                }
            }
        });

        jQuery('.dash_ans_opt').click(function () {
            var show_data_ans = jQuery(this).parents('.card-body').find('.answer_dash').attr('ans-data');
            jQuery(this).toggleClass('badge-success badge-danger');
            if (jQuery(this).html() == '<i class="fa fa-eye" aria-hidden="true"></i>ShowAns') {
                jQuery(this).html('<i class="fa fa-eye-slash" aria-hidden="true"></i>HideAns');
                jQuery(this).parents('.card-body').find('.answer_dash input').hide(1000, function () {
                    jQuery(this).parents('.card-body').find('.hide_dash_ans').show(1000);
                });

            } else {
                jQuery(this).html('<i class="fa fa-eye" aria-hidden="true"></i>ShowAns');
                jQuery(this).parents('.card-body').find('.hide_dash_ans').hide(1000);
                //jQuery(this).parents('.card-body').find('.answer_dash input').removeAttr('disabled').css('border-color', '#ced4da').val('').show(1000);
                jQuery(this).parents('.card-body').find('.answer_dash input').show(1000);

            }
        });

        jQuery('.ansonclick').find('.answer').click(function () {
            jQuery(this).parents('.ansonclick').css('pointer-events', 'none');
            jQuery(this).parents('.card-body').find('.check_ans_opt').show(1000);
        });
        jQuery('.ansonclick').find('.rightAns').click(function () {
            positive = positive + 1;
            total = positive - nagative;
            jQuery('#positive').html(positive);
            jQuery('#total').html(total);
            if (total < total_ques * .55) {
                jQuery('.temp-result').css('background-color', 'red');
            } else {
                jQuery('.temp-result').css('background-color', 'green');
            }
        });
        jQuery('.ansonclick').find('.wrongAns').click(function () {
            nagative = nagative + .25;
            nagative_count = nagative_count + 1;
            total = positive - nagative;
            jQuery('#nagative').html(nagative_count);
            jQuery('#total').html(total);
            if (total < total_ques * .55) {
                jQuery('.temp-result').css('background-color', 'red');
            } else {
                jQuery('.temp-result').css('background-color', 'green');
            }
        });
    });
    jQuery(window).scroll(function () {
        if (jQuery(this).scrollTop() > 55) {
            jQuery('#checkallans').addClass('btn-fixed');
        } else {
            jQuery('#checkallans').removeClass('btn-fixed');
        }
    });


</script>


