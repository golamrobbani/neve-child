<?php
/**
 * Author:          Andrei Baicus <andrei@themeisle.com>
 * Created on:      28/08/2018
 *
 * @package Neve
 */
$container_class = apply_filters('neve_container_class_filter', 'container', 'single-post');
get_header();
?>
    <div class="<?php echo esc_attr($container_class); ?> single-post-container">
        <div class="row">
            <div class="nv-sidebar-wrap nv-child-sidebar col-sm-12 nv-left blog-sidebar">

                <?php
                $args = array(
                    'orderby' => 'name',
                    'hide_empty' => 0,
                );
                $terms = get_terms('jobqs_category', $args);
                //wp_list_pluck($terms, 'name', 'id');
                if (!empty($terms) && !is_wp_error($terms)) {
                    $count     = count($terms);
                    $i         = 0;
                    $term_list = '<div class="c-content-ver-nav">
                      <div class="c-content-title-1 c-theme c-title-md c-margin-t-40">
                        <h3 class="c-font-bold c-font-uppercase">Categories</h3>
                        <div class="c-line-left c-theme-bg"></div>
                      </div>
                      <ul class="c-menu c-arrow-dot1 c-theme">';
                    foreach ($terms as $term) {
                        $i++;
                        $term_list .= '<li>';
                        $term_list .= '<a href="' . esc_url(get_term_link($term)) . '" alt="' . esc_attr(sprintf(__('View all post filed under %s', 'my_localization_domain'), $term->name)) . '">' . $term->name.'</a>';
                       /* if ($count != $i) {
                            $term_list .= '<span class="icon"><i class="fa fa-angle-right"></i></span>';
                        }*/
                        $term_list .= '</li>';
                    }
                    $term_list .= '</ul></div>';
                    echo $term_list;
                }
              ?>
            </div>

            <?php //do_action( 'neve_do_sidebar', 'single-post', 'left' ); ?>
            <article id="post-<?php echo esc_attr(get_the_ID()); ?>"
                     class="<?php echo esc_attr(join(' ', get_post_class('nv-single-post-wrap nv-child-single-post-wrap col'))); ?>">
                <?php
                do_action('neve_before_post_content');
                if (have_posts()) {
                    while (have_posts()) {
                        the_post();

                        get_template_part('template-parts/content', 'single-jobqs');
                    }
                } else {
                    get_template_part('template-parts/content', 'none');
                }
                do_action('neve_after_post_content');
                ?>
            </article>

            <?php //do_action( 'neve_do_sidebar', 'single-post', 'right' ); ?>
            <?php if ( is_active_sidebar( 'nv-child-right-sidebar' ) ) {?>
                <div class="nv-sidebar-wrap nv-child-sidebar col-sm-12 nv-right blog-sidebar">
                    <?php dynamic_sidebar('nv-child-right-sidebar'); ?>
                </div>
            <?php } ?>

        </div>
    </div>
<?php
get_footer();
