<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
/**
 * Load CSS file.
 */
if ( ! function_exists( 'neve_child_load_css' ) ):
	function neve_child_load_css() {
        wp_enqueue_style( 'neve-child-font-icons', trailingslashit( get_stylesheet_directory_uri() ) . 'assets/css/font-awesome.min.css', array(), NEVE_VERSION );
        wp_enqueue_style( 'neve-child-archive-style', trailingslashit( get_stylesheet_directory_uri() ) . 'assets/css/archive-style.css', array(), NEVE_VERSION );
        wp_enqueue_style( 'neve-child-style', trailingslashit( get_stylesheet_directory_uri() ) . 'style.css', array( 'neve-style' ), NEVE_VERSION );
        wp_enqueue_script('jquery');
    }
endif;
add_action( 'wp_enqueue_scripts', 'neve_child_load_css', 20 );
require get_stylesheet_directory() . '/inc/register-widgets.php';
require get_stylesheet_directory() . '/inc/custom-post-type/register-post-taxonomy-metabox.php';
