<?php
function nv_child_widgets_init() {
	/*register_sidebar( array(
		'name'          => esc_html__( 'Left Sidebar', 'neve' ),
		'id'            => 'nv-child-left-sidebar',
		'description'   => esc_html__( 'Add widgets here.', 'neve' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );*/

	register_sidebar( array(
		'name'          => esc_html__( 'Right Sidebar', 'neve' ),
		'id'            => 'nv-child-right-sidebar',
		'description'   => esc_html__( 'Add widgets here.', 'neve' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<p class="widget-title">',
		'after_title'   => '</p>',
	) );
}

add_action( 'widgets_init', 'nv_child_widgets_init' );