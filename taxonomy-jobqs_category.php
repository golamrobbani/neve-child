<?php
/**
 * Index template.
 *
 * @package Neve
 */
$container_class = apply_filters( 'neve_container_class_filter', 'container', 'blog-archive' );
get_header();
?>

    <div class="<?php echo esc_attr( $container_class ); ?> archive-container">
        <div class="row">
            <div class="nv-sidebar-wrap nv-child-sidebar col-sm-12 nv-left blog-sidebar">
                <?php
                $args = array(
                    'orderby' => 'name',
                    'hide_empty' => 0,
                );
                $terms = get_terms('jobqs_category', $args);
                //wp_list_pluck($terms, 'name', 'id');
                if (!empty($terms) && !is_wp_error($terms)) {
                    $count     = count($terms);
                    $i         = 0;
                    $term_list = '<div class="c-content-ver-nav">
                      <div class="c-content-title-1 c-theme c-title-md c-margin-t-40">
                        <h3 class="c-font-bold c-font-uppercase">Categories</h3>
                        <div class="c-line-left c-theme-bg"></div>
                      </div>
                      <ul class="c-menu c-arrow-dot1 c-theme">';
                    foreach ($terms as $term) {
                        $i++;
                        $term_list .= '<li>';
                        $term_list .= '<a href="' . esc_url(get_term_link($term)) . '" alt="' . esc_attr(sprintf(__('View all post filed under %s', 'my_localization_domain'), $term->name)) . '">' . $term->name.'</a>';
                        /* if ($count != $i) {
                             $term_list .= '<span class="icon"><i class="fa fa-angle-right"></i></span>';
                         }*/
                        $term_list .= '</li>';
                    }
                    $term_list .= '</ul></div>';
                    echo $term_list;
                }
                ?>
            </div>

            <?php //do_action( 'neve_do_sidebar', 'blog-archive', 'left' ); ?>
            <div class="nv-index-posts nv-child-index-posts blog col">
                <?php
                do_action( 'neve_before_loop' );
                do_action( 'neve_page_header', 'index' );
                do_action( 'neve_before_posts_loop' );
                if ( have_posts() ) {
                    /* Start the Loop. */
                    echo '<div class="posts-wrapper row">';
                    $pagination_type = get_theme_mod( 'neve_pagination_type', 'number' );
                    if ( $pagination_type !== 'infinite' ) {
                        global $wp_query;

                        $posts_on_current_page = $wp_query->post_count;
                        $hook_after_post       = -1;

                        if ( $posts_on_current_page >= 2 ) {
                            $hook_after_post = intval( ceil( $posts_on_current_page / 2 ) );
                        }
                        $post_index = 1;
                    }
                    while ( have_posts() ) {
                        the_post();
                        get_template_part( 'template-parts/content', get_post_type() );

                        if ( $pagination_type !== 'infinite' ) {
                            if ( $post_index === $hook_after_post && $hook_after_post !== - 1 ) {
                                do_action( 'neve_middle_posts_loop' );
                            }
                            $post_index ++;
                        }
                    }
                    echo '</div>';
                    if ( ! is_singular() ) {
                        do_action( 'neve_do_pagination', 'blog-archive' );
                    }
                } else {
                    get_template_part( 'template-parts/content', 'none' );
                }
                ?>
                <div class="w-100"></div>
                <?php do_action( 'neve_after_posts_loop' ); ?>
            </div>
            <?php ///do_action( 'neve_do_sidebar', 'blog-archive', 'right' ); ?>

            <?php if ( is_active_sidebar( 'nv-child-right-sidebar' ) ) {?>
                <div class="nv-sidebar-wrap nv-child-sidebar col-sm-12 nv-right blog-sidebar">
                    <?php dynamic_sidebar('nv-child-right-sidebar'); ?>
                </div>
            <?php } ?>

        </div>
    </div>
<?php
get_footer();
